<?php

/**
 * @see /commerce_single_page/includes/process.interface.inc for documentation
 * on the structure behind each function below 
 */
class CommerceSinglePageProcessGCADonate implements CommerceSinglePageProcessInterface {

  public function splitCheckoutInfo() {
    return array(
      'path' => 'donate',
      'menu' => array(
        'title' => 'Donate',
      )
    );
  }
  
  /**
   * This points to a required line item type defined in commerce_donate.module
   */
  public function getSinglePageLineItemType() {
    return "commerce_donate";
  }
  
  public function hasRelatedProduct() {
    return TRUE;
  }
  
  public function getRelatedProductType() {
    return "donation";
  }
  
  public function getRelatedProductDisplayID() {
    return 1;
  }
  
  public function getVerbiage($for) {
    switch($for) {
      case "add_to_cart_action":
        return "Donate";
      default:
        return FALSE;
    }
  }
  
  public function alterAllCheckoutForms(&$form, &$form_state) {
    // "N/A" or "none" breaks this line item, set it to 0 as default and remove "none" option
    unset($form['checkout_donate']['checkout_donate']['commerce_donate_amount']['und']['#options']['none']);
    $form['checkout_donate']['checkout_donate']['commerce_donate_amount']['und']['#default_value'] = array('value' => '0');
  }
  
  public function alterSinglePageCheckoutForm(&$form, &$form_state) {
    //set up one-page donation for "donate cmode"
    drupal_set_title("Make a donation");
    //remove "cart contents"
    unset($form['cart_contents']);
    //unset "0" option
    unset($form['checkout_donate']['checkout_donate']['commerce_donate_amount']['und']['#options']['0']);
    $form['checkout_donate']['checkout_donate']['commerce_donate_amount']['und']['#default_value'] = array('value' => '25');
  }
  
  public function alterNonSinglePageCheckoutForms(&$form, &$form_state) {
    //change title of donation field for regular non-"donate cmode" checkouts
    $form['checkout_donate']['#title'] = "Add a donation to your order";
    //$form['#submit'][] = "_commerce_single_page_checkout_now";
  }
  
  
  public function getSplitPageCheckoutMessage($quantity) {
    $isare = $quantity>1?"are":"is";
    $itemitems = $quantity>1?"items":"item";
    
    //build message to user
    $message = "
      <p>There $isare currently $quantity $itemitems in your shopping cart.  You have 2 options:</p>
      <ol>
        <li>Add a donation to your cart at checkout and pay for your cart items and donation one transaction.</li>
        <li>Save your shopping cart $itemitems for later and pay only for the donation now.</li>
      </ol>
      <p>How would you like to proceed?</p>
     ";
     
     return $message;
  }
  
  public function alterSplitPageCheckoutForm(&$form, &$form_state) {
    //alter button text
    $form['buttons']['add_to_cart']['#value'] = "Add donation at checkout";
  }
  
  public function alterSplitPageCheckoutReviewForm(&$form, &$form_state) {
    $form['checkout_review']['review']['#data']['cart_contents']['title'] = t("Payment summary");
  }
  
  public function splitCheckoutAddAction() {
    return "_commerce_single_page_checkout_now";
  }
  
}