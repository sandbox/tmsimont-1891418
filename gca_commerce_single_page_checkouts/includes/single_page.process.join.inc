<?php

/**
 * @see /commerce_single_page/includes/process.interface.inc for documentation
 * on the structure behind each function below 
 */
class CommerceSinglePageProcessGCAJoin implements CommerceSinglePageProcessInterface {

  public function splitCheckoutInfo() {
    return array(
      'path' => 'join',
      'menu' => array(
        'title' => 'Become a Member',
      )
    );
  }
  
  /**
   * This points to a required line item type defined in commerce_donate.module
   */
  public function getSinglePageLineItemType() {
    return "gca_commerce_membership";
  }
  
  public function hasRelatedProduct() {
    return TRUE;
  }
  
  public function getRelatedProductType() {
    return "membership";
  }
  
  public function getRelatedProductDisplayID() {
    return 165;
  }
  
  public function getVerbiage($for) {
    switch($for) {
      case "add_to_cart_action":
        return "Add membership to cart";
      default:
        return FALSE;
    }
  }
  
  public function alterAllCheckoutForms(&$form, &$form_state) {
  }
  
  public function alterSinglePageCheckoutForm(&$form, &$form_state) {
    //set up one-page checkout for "join cmode"
    drupal_set_title("Become a member");
    //remove "cart contents"
    unset($form['cart_contents']);
    
    //add class so label can be hidden in CSS
    $form['checkout_join']['checkout_join']['gca_commerce_membership_amount']['#attributes']['class'][] = "gca-membership-one-page-form";
    //unset "none" option
    unset($form['checkout_join']['checkout_join']['gca_commerce_membership_amount']['und']['#options']['none']);
    //set default to 35|Basic Membership
    $form['checkout_join']['checkout_join']['gca_commerce_membership_amount']['und']['#default_value'] = 35;
  }
  
  public function alterNonSinglePageCheckoutForms(&$form, &$form_state) {
    //remove the membership fields from non "join cmode" checkouts
    unset($form['checkout_join']);
  }
  
  
  public function getSplitPageCheckoutMessage($quantity) {
    $isare = $quantity>1?"are":"is";
    $itemitems = $quantity>1?"items":"item";
      
    //build message to user
    $message = "
      <p>There $isare currently $quantity $itemitems in your shopping cart.  You have 2 options:</p>
      <ol>
        <li>Add a membership to your cart and pay for both your membership and your cart items in one transaction.</li>
        <li>Save your shopping cart $itemitems for later and pay only for a membership now.</li>
      </ol>
      <p>How would you like to proceed?</p>
     ";
     
     return $message;
  }
  
  public function alterSplitPageCheckoutForm(&$form, &$form_state) {
    //alter button text
    $form['buttons']['add_to_cart']['#value'] = "Add membership to cart";
  }
  
  public function alterSplitPageCheckoutReviewForm(&$form, &$form_state) {
    $form['checkout_review']['review']['#data']['cart_contents']['title'] = t("Payment summary");
  }
  
  public function splitCheckoutAddAction() {
    return "_commerce_split_checkout_split_add_to_cart";
  }
}