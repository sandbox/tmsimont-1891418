<?php


class CommerceSinglePageProcessDefault implements CommerceSinglePageProcessInterface {

  public function splitCheckoutInfo() {
  }
  
  public function hasRelatedProduct(){
  }
  
  public function getRelatedProductType(){
  }
  
  public function getRelatedProductDisplayID(){
  }
  
  public function getVerbiage($for){
  }
  
  public function alterCheckoutForms(&$form, &$form_state){
  }
  
  public function alterSinglePageCheckoutForm(&$form, &$form_state){
  }
  
  public function getSplitPageCheckoutMessage(){
  }
  
  public function alterSplitPageCheckoutForm(&$form, &$form_state){
  }
  
  public function splitCheckoutAddAction() {
    return "_commerce_single_page_checkout_now";
  }
  
}