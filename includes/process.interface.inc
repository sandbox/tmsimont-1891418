<?php

interface CommerceSinglePageProcessInterface {
  /**
   * Return data for hook_commerce_split_checkout_mode_info().
   * @return array() (see commerce_split_checkout)
   */
  public function splitCheckoutInfo();
  
  /**
   * Return the machine name of the single page line item that 
   * will appear on the single page checkout
   * @return string
   *   The machine name of the line item
   */
  public function getSinglePageLineItemType();

  /**
  
  /**
   * Tells the system if there is a product display containing the same 
   * product line item that is on the single page checkout form.
   * @return bool
   *   TRUE if this has a related product type and display, FALSE if not.
   */
  public function hasRelatedProduct();

  /**
   * Returns the product type machine name for the product type that has
   * the same product line item that is on the single page checkout form.
   * @return string
   *   The machine name of the product type.
   */  
  public function getRelatedProductType();
    
  /**
   * Returns the product display node idfor the product display that has
   * the same product line item that is on the single page checkout form.
   * @return int
   *   The node id of the product display ID
   */  
  public function getRelatedProductDisplayID();
  
  /** 
   * Given a use-case, return verbiage for a specific part of the process
   * @param string $for
   *   A string that defines the use case.
   * @return string 
   *   The translation or override for the given use case, should return 
   *   FALSE if there's no need to override
   */
  public function getVerbiage($for);
  
  /**
   * Allows implementation to alter all checkout forms
   * @param $form
   *   reference to form array in hook_form_alter()
   * @param $form_state
   *   reference to form_state array in hook_form_alter()
   */
  public function alterAllCheckoutForms(&$form, &$form_state);
  
  /**
   * Allows implementation to alter the "single item" checkout 
   * form only and not all the other checkout forms
   * @param $form
   *   reference to form array in hook_form_alter()
   * @param $form_state
   *   reference to form_state array in hook_form_alter()
   */
  public function alterSinglePageCheckoutForm(&$form, &$form_state);
  
  /**
   * Allows implementation to alter the checkout forms 
   * that are not the "single item" checkout form
   * @param $form
   *   reference to form array in hook_form_alter()
   * @param $form_state
   *   reference to form_state array in hook_form_alter()
   */
  public function alterNonSinglePageCheckoutForms(&$form, &$form_state);
  
  /**
   * Allows implementation to return a message to the user above the
   * form that gives options to the user on how to proceed
   * when the single page checkout is hit when items are already
   * in the user's cart.
   * @param $quantity
   *   The number of items in the cart
   */
  public function getSplitPageCheckoutMessage($quantity);
  
    
  /**
   * Allows implementation to alter the "split page" checkout 
   * form that gives options to the user on how to proceed
   * when the single page checkout is hit when items are already
   * in the user's cart.
   * @param $form
   *   reference to form array in hook_form_alter()
   * @param $form_state
   *   reference to form_state array in hook_form_alter()
   */
  public function alterSplitPageCheckoutForm(&$form, &$form_state);
  
  
  /**
   * Allows implementation to alter the review checkout form 
   * when the order contains only the single page item
   * @param $form
   *   reference to form array in hook_form_alter()
   * @param $form_state
   *   reference to form_state array in hook_form_alter()
   */
  public function alterSplitPageCheckoutReviewForm(&$form, &$form_state);
  
  /**
   * Return the callback name for the "add to cart" action
   */
  public function splitCheckoutAddAction();
}